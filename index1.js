/**
 * Created by kazan on 07.06.2016.
 */
//var brain = require('brain')
//var net = new brain.NeuralNetwork();
//
//net.train([{input: {r: 0.03, g: 0.7, b: 0.5}, output: {black: 1}},
//    {input: {r: 0.16, g: 0.09, b: 0.2}, output: {white: 1}},
//    {input: {r: 0.5, g: 0.5, b: 1.0}, output: {white: 1}}]);
//
//var output = net.run({r: 1, g: 0.4, b: 0});  // { white: 0.99, black: 0.002 }
//console.log(net);


var sorceData = [
    {
        "leaf-length": 5.1,
        "leaf-width": 3.5,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 3,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.7,
        "leaf-width": 3.2,
        "petal-length": 1.3,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.6,
        "leaf-width": 3.1,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.6,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.9,
        "petal-length": 1.7,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.6,
        "leaf-width": 3.4,
        "petal-length": 1.4,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.4,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.4,
        "leaf-width": 2.9,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 3.1,
        "petal-length": 1.5,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.7,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3.4,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3,
        "petal-length": 1.4,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.3,
        "leaf-width": 3,
        "petal-length": 1.1,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 4,
        "petal-length": 1.2,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 4.4,
        "petal-length": 1.5,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.9,
        "petal-length": 1.3,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.5,
        "petal-length": 1.4,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 3.8,
        "petal-length": 1.7,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.8,
        "petal-length": 1.5,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.4,
        "petal-length": 1.7,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.7,
        "petal-length": 1.5,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.6,
        "leaf-width": 3.6,
        "petal-length": 1,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.3,
        "petal-length": 1.7,
        "petal-width": 0.5,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3.4,
        "petal-length": 1.9,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.4,
        "petal-length": 1.6,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.2,
        "leaf-width": 3.5,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.2,
        "leaf-width": 3.4,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.7,
        "leaf-width": 3.2,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3.1,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.4,
        "petal-length": 1.5,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.2,
        "leaf-width": 4.1,
        "petal-length": 1.5,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 4.2,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 3.1,
        "petal-length": 1.5,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.2,
        "petal-length": 1.2,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 3.5,
        "petal-length": 1.3,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 3.1,
        "petal-length": 1.5,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.4,
        "leaf-width": 3,
        "petal-length": 1.3,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.4,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.5,
        "petal-length": 1.3,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.5,
        "leaf-width": 2.3,
        "petal-length": 1.3,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.4,
        "leaf-width": 3.2,
        "petal-length": 1.3,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.5,
        "petal-length": 1.6,
        "petal-width": 0.6,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.8,
        "petal-length": 1.9,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3,
        "petal-length": 1.4,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.8,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.6,
        "leaf-width": 3.2,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.3,
        "leaf-width": 3.7,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.3,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 7,
        "leaf-width": 3.2,
        "petal-length": 4.7,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 3.2,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.9,
        "leaf-width": 3.1,
        "petal-length": 4.9,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.3,
        "petal-length": 4,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 2.8,
        "petal-length": 4.6,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.8,
        "petal-length": 4.5,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 3.3,
        "petal-length": 4.7,
        "petal-width": 1.6,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 2.4,
        "petal-length": 3.3,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.6,
        "leaf-width": 2.9,
        "petal-length": 4.6,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.2,
        "leaf-width": 2.7,
        "petal-length": 3.9,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5,
        "leaf-width": 2,
        "petal-length": 3.5,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.9,
        "leaf-width": 3,
        "petal-length": 4.2,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6,
        "leaf-width": 2.2,
        "petal-length": 4,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 2.9,
        "petal-length": 4.7,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 2.9,
        "petal-length": 3.6,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.1,
        "petal-length": 4.4,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 3,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.7,
        "petal-length": 4.1,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.2,
        "leaf-width": 2.2,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 2.5,
        "petal-length": 3.9,
        "petal-width": 1.1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.9,
        "leaf-width": 3.2,
        "petal-length": 4.8,
        "petal-width": 1.8,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 2.8,
        "petal-length": 4,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.5,
        "petal-length": 4.9,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 2.8,
        "petal-length": 4.7,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 2.9,
        "petal-length": 4.3,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.6,
        "leaf-width": 3,
        "petal-length": 4.4,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.8,
        "leaf-width": 2.8,
        "petal-length": 4.8,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3,
        "petal-length": 5,
        "petal-width": 1.7,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6,
        "leaf-width": 2.9,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.6,
        "petal-length": 3.5,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.4,
        "petal-length": 3.8,
        "petal-width": 1.1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.4,
        "petal-length": 3.7,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.7,
        "petal-length": 3.9,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6,
        "leaf-width": 2.7,
        "petal-length": 5.1,
        "petal-width": 1.6,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6,
        "leaf-width": 3.4,
        "petal-length": 4.5,
        "petal-width": 1.6,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.1,
        "petal-length": 4.7,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.3,
        "petal-length": 4.4,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 3,
        "petal-length": 4.1,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.5,
        "petal-length": 4,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.6,
        "petal-length": 4.4,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 3,
        "petal-length": 4.6,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.6,
        "petal-length": 4,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5,
        "leaf-width": 2.3,
        "petal-length": 3.3,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 2.7,
        "petal-length": 4.2,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 3,
        "petal-length": 4.2,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.9,
        "petal-length": 4.2,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.2,
        "leaf-width": 2.9,
        "petal-length": 4.3,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 2.5,
        "petal-length": 3,
        "petal-width": 1.1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.8,
        "petal-length": 4.1,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 3.3,
        "petal-length": 6,
        "petal-width": 2.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.7,
        "petal-length": 5.1,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.1,
        "leaf-width": 3,
        "petal-length": 5.9,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.9,
        "petal-length": 5.6,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 3,
        "petal-length": 5.8,
        "petal-width": 2.2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.6,
        "leaf-width": 3,
        "petal-length": 6.6,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 2.5,
        "petal-length": 4.5,
        "petal-width": 1.7,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.3,
        "leaf-width": 2.9,
        "petal-length": 6.3,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 2.5,
        "petal-length": 5.8,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.2,
        "leaf-width": 3.6,
        "petal-length": 6.1,
        "petal-width": 2.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 3.2,
        "petal-length": 5.1,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 2.7,
        "petal-length": 5.3,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.8,
        "leaf-width": 3,
        "petal-length": 5.5,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.5,
        "petal-length": 5,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.8,
        "petal-length": 5.1,
        "petal-width": 2.4,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 3.2,
        "petal-length": 5.3,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 3,
        "petal-length": 5.5,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.7,
        "leaf-width": 3.8,
        "petal-length": 6.7,
        "petal-width": 2.2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.7,
        "leaf-width": 2.6,
        "petal-length": 6.9,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6,
        "leaf-width": 2.2,
        "petal-length": 5,
        "petal-width": 1.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.9,
        "leaf-width": 3.2,
        "petal-length": 5.7,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 2.8,
        "petal-length": 4.9,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.7,
        "leaf-width": 2.8,
        "petal-length": 6.7,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.7,
        "petal-length": 4.9,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.3,
        "petal-length": 5.7,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.2,
        "leaf-width": 3.2,
        "petal-length": 6,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.2,
        "leaf-width": 2.8,
        "petal-length": 4.8,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 3,
        "petal-length": 4.9,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 2.8,
        "petal-length": 5.6,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.2,
        "leaf-width": 3,
        "petal-length": 5.8,
        "petal-width": 1.6,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.4,
        "leaf-width": 2.8,
        "petal-length": 6.1,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.9,
        "leaf-width": 3.8,
        "petal-length": 6.4,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 2.8,
        "petal-length": 5.6,
        "petal-width": 2.2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.8,
        "petal-length": 5.1,
        "petal-width": 1.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 2.6,
        "petal-length": 5.6,
        "petal-width": 1.4,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.7,
        "leaf-width": 3,
        "petal-length": 6.1,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 3.4,
        "petal-length": 5.6,
        "petal-width": 2.4,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 3.1,
        "petal-length": 5.5,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6,
        "leaf-width": 3,
        "petal-length": 4.8,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.9,
        "leaf-width": 3.1,
        "petal-length": 5.4,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.1,
        "petal-length": 5.6,
        "petal-width": 2.4,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.9,
        "leaf-width": 3.1,
        "petal-length": 5.1,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.7,
        "petal-length": 5.1,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.8,
        "leaf-width": 3.2,
        "petal-length": 5.9,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.3,
        "petal-length": 5.7,
        "petal-width": 2.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3,
        "petal-length": 5.2,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.5,
        "petal-length": 5,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 3,
        "petal-length": 5.2,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.2,
        "leaf-width": 3.4,
        "petal-length": 5.4,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.9,
        "leaf-width": 3,
        "petal-length": 5.1,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    }
];

var synaptic = require('synaptic'); // this line is not needed in the browser
var Neuron = synaptic.Neuron,
    Layer = synaptic.Layer,
    Network = synaptic.Network,
    Trainer = synaptic.Trainer,
    Architect = synaptic.Architect;

// create the network
var inputLayer = new Layer(4);
var hiddenLayer = new Layer(12);
var outputLayer = new Layer(3);

inputLayer.project(hiddenLayer);
hiddenLayer.project(outputLayer);

var myNetwork = new Network({
    input: inputLayer,
    hidden: [hiddenLayer],
    output: outputLayer
});
var corellationTable = {
    "Iris-virginica": 3,
    "Iris-versicolor": 2,
    "Iris-setosa": 1}
var trainerSet = [];
sorceData.forEach(function (item) {
    var arr = [0,0,0];
    arr[corellationTable[item.class]] = 1;
    trainerSet.push({
        input: [
            item["leaf-length"],
            item["leaf-width"],
            item["petal-length"],
            item["petal-width"]
        ],
        output: arr
    })
});

// train the network
var trainer = new Trainer(myNetwork);
console.log(trainerSet);
trainer.train(trainerSet);

// test the network
console.log(myNetwork.activate([
    5.9,
    3,
    5.1,
    1.8,
]));
console.log(myNetwork.layers)
//myNetwork.activate([0,1]); // [0.9815816381088985]
//myNetwork.activate([1,0]); // [0.9871822457132193]
//myNetwork.activate([1,1]); // [0.012950087641929467]
//console.log(myNetwork.neurons());
//var brain = require('brain')
//var net = new brain.NeuralNetwork();
//    net.train(trainerSet);
//
//var output = net.run([
//   4.7,
//   3.2,
//   1.3,
//   0.2,
//]);  // { white: 0.99, black: 0.002 }
//console.log(output);
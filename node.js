/**
 * Created by kazan on 07.06.2016.
 */
/**
 * Created by kazan on 07.06.2016.
 */

var sorceData = [
    {
        "leaf-length": 5.1,
        "leaf-width": 3.5,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 3,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.7,
        "leaf-width": 3.2,
        "petal-length": 1.3,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.6,
        "leaf-width": 3.1,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.6,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.9,
        "petal-length": 1.7,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.6,
        "leaf-width": 3.4,
        "petal-length": 1.4,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.4,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.4,
        "leaf-width": 2.9,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 3.1,
        "petal-length": 1.5,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.7,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3.4,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3,
        "petal-length": 1.4,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.3,
        "leaf-width": 3,
        "petal-length": 1.1,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 4,
        "petal-length": 1.2,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 4.4,
        "petal-length": 1.5,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.9,
        "petal-length": 1.3,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.5,
        "petal-length": 1.4,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 3.8,
        "petal-length": 1.7,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.8,
        "petal-length": 1.5,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.4,
        "petal-length": 1.7,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.7,
        "petal-length": 1.5,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.6,
        "leaf-width": 3.6,
        "petal-length": 1,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.3,
        "petal-length": 1.7,
        "petal-width": 0.5,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3.4,
        "petal-length": 1.9,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.4,
        "petal-length": 1.6,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.2,
        "leaf-width": 3.5,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.2,
        "leaf-width": 3.4,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.7,
        "leaf-width": 3.2,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3.1,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3.4,
        "petal-length": 1.5,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.2,
        "leaf-width": 4.1,
        "petal-length": 1.5,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 4.2,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 3.1,
        "petal-length": 1.5,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.2,
        "petal-length": 1.2,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 3.5,
        "petal-length": 1.3,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 3.1,
        "petal-length": 1.5,
        "petal-width": 0.1,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.4,
        "leaf-width": 3,
        "petal-length": 1.3,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.4,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.5,
        "petal-length": 1.3,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.5,
        "leaf-width": 2.3,
        "petal-length": 1.3,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.4,
        "leaf-width": 3.2,
        "petal-length": 1.3,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.5,
        "petal-length": 1.6,
        "petal-width": 0.6,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.8,
        "petal-length": 1.9,
        "petal-width": 0.4,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.8,
        "leaf-width": 3,
        "petal-length": 1.4,
        "petal-width": 0.3,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 3.8,
        "petal-length": 1.6,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 4.6,
        "leaf-width": 3.2,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5.3,
        "leaf-width": 3.7,
        "petal-length": 1.5,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 5,
        "leaf-width": 3.3,
        "petal-length": 1.4,
        "petal-width": 0.2,
        "class": "Iris-setosa"
    },
    {
        "leaf-length": 7,
        "leaf-width": 3.2,
        "petal-length": 4.7,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 3.2,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.9,
        "leaf-width": 3.1,
        "petal-length": 4.9,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.3,
        "petal-length": 4,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 2.8,
        "petal-length": 4.6,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.8,
        "petal-length": 4.5,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 3.3,
        "petal-length": 4.7,
        "petal-width": 1.6,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 2.4,
        "petal-length": 3.3,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.6,
        "leaf-width": 2.9,
        "petal-length": 4.6,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.2,
        "leaf-width": 2.7,
        "petal-length": 3.9,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5,
        "leaf-width": 2,
        "petal-length": 3.5,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.9,
        "leaf-width": 3,
        "petal-length": 4.2,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6,
        "leaf-width": 2.2,
        "petal-length": 4,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 2.9,
        "petal-length": 4.7,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 2.9,
        "petal-length": 3.6,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.1,
        "petal-length": 4.4,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 3,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.7,
        "petal-length": 4.1,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.2,
        "leaf-width": 2.2,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 2.5,
        "petal-length": 3.9,
        "petal-width": 1.1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.9,
        "leaf-width": 3.2,
        "petal-length": 4.8,
        "petal-width": 1.8,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 2.8,
        "petal-length": 4,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.5,
        "petal-length": 4.9,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 2.8,
        "petal-length": 4.7,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 2.9,
        "petal-length": 4.3,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.6,
        "leaf-width": 3,
        "petal-length": 4.4,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.8,
        "leaf-width": 2.8,
        "petal-length": 4.8,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3,
        "petal-length": 5,
        "petal-width": 1.7,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6,
        "leaf-width": 2.9,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.6,
        "petal-length": 3.5,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.4,
        "petal-length": 3.8,
        "petal-width": 1.1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.4,
        "petal-length": 3.7,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.7,
        "petal-length": 3.9,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6,
        "leaf-width": 2.7,
        "petal-length": 5.1,
        "petal-width": 1.6,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.4,
        "leaf-width": 3,
        "petal-length": 4.5,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6,
        "leaf-width": 3.4,
        "petal-length": 4.5,
        "petal-width": 1.6,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.1,
        "petal-length": 4.7,
        "petal-width": 1.5,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.3,
        "petal-length": 4.4,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 3,
        "petal-length": 4.1,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.5,
        "petal-length": 4,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.5,
        "leaf-width": 2.6,
        "petal-length": 4.4,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 3,
        "petal-length": 4.6,
        "petal-width": 1.4,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.6,
        "petal-length": 4,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5,
        "leaf-width": 2.3,
        "petal-length": 3.3,
        "petal-width": 1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 2.7,
        "petal-length": 4.2,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 3,
        "petal-length": 4.2,
        "petal-width": 1.2,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.9,
        "petal-length": 4.2,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.2,
        "leaf-width": 2.9,
        "petal-length": 4.3,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.1,
        "leaf-width": 2.5,
        "petal-length": 3,
        "petal-width": 1.1,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.8,
        "petal-length": 4.1,
        "petal-width": 1.3,
        "class": "Iris-versicolor"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 3.3,
        "petal-length": 6,
        "petal-width": 2.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.7,
        "petal-length": 5.1,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.1,
        "leaf-width": 3,
        "petal-length": 5.9,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.9,
        "petal-length": 5.6,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 3,
        "petal-length": 5.8,
        "petal-width": 2.2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.6,
        "leaf-width": 3,
        "petal-length": 6.6,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 4.9,
        "leaf-width": 2.5,
        "petal-length": 4.5,
        "petal-width": 1.7,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.3,
        "leaf-width": 2.9,
        "petal-length": 6.3,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 2.5,
        "petal-length": 5.8,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.2,
        "leaf-width": 3.6,
        "petal-length": 6.1,
        "petal-width": 2.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 3.2,
        "petal-length": 5.1,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 2.7,
        "petal-length": 5.3,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.8,
        "leaf-width": 3,
        "petal-length": 5.5,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.7,
        "leaf-width": 2.5,
        "petal-length": 5,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.8,
        "petal-length": 5.1,
        "petal-width": 2.4,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 3.2,
        "petal-length": 5.3,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 3,
        "petal-length": 5.5,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.7,
        "leaf-width": 3.8,
        "petal-length": 6.7,
        "petal-width": 2.2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.7,
        "leaf-width": 2.6,
        "petal-length": 6.9,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6,
        "leaf-width": 2.2,
        "petal-length": 5,
        "petal-width": 1.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.9,
        "leaf-width": 3.2,
        "petal-length": 5.7,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.6,
        "leaf-width": 2.8,
        "petal-length": 4.9,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.7,
        "leaf-width": 2.8,
        "petal-length": 6.7,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.7,
        "petal-length": 4.9,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.3,
        "petal-length": 5.7,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.2,
        "leaf-width": 3.2,
        "petal-length": 6,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.2,
        "leaf-width": 2.8,
        "petal-length": 4.8,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 3,
        "petal-length": 4.9,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 2.8,
        "petal-length": 5.6,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.2,
        "leaf-width": 3,
        "petal-length": 5.8,
        "petal-width": 1.6,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.4,
        "leaf-width": 2.8,
        "petal-length": 6.1,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.9,
        "leaf-width": 3.8,
        "petal-length": 6.4,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 2.8,
        "petal-length": 5.6,
        "petal-width": 2.2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.8,
        "petal-length": 5.1,
        "petal-width": 1.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.1,
        "leaf-width": 2.6,
        "petal-length": 5.6,
        "petal-width": 1.4,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 7.7,
        "leaf-width": 3,
        "petal-length": 6.1,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 3.4,
        "petal-length": 5.6,
        "petal-width": 2.4,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.4,
        "leaf-width": 3.1,
        "petal-length": 5.5,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6,
        "leaf-width": 3,
        "petal-length": 4.8,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.9,
        "leaf-width": 3.1,
        "petal-length": 5.4,
        "petal-width": 2.1,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.1,
        "petal-length": 5.6,
        "petal-width": 2.4,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.9,
        "leaf-width": 3.1,
        "petal-length": 5.1,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.8,
        "leaf-width": 2.7,
        "petal-length": 5.1,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.8,
        "leaf-width": 3.2,
        "petal-length": 5.9,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3.3,
        "petal-length": 5.7,
        "petal-width": 2.5,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.7,
        "leaf-width": 3,
        "petal-length": 5.2,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.3,
        "leaf-width": 2.5,
        "petal-length": 5,
        "petal-width": 1.9,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.5,
        "leaf-width": 3,
        "petal-length": 5.2,
        "petal-width": 2,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 6.2,
        "leaf-width": 3.4,
        "petal-length": 5.4,
        "petal-width": 2.3,
        "class": "Iris-virginica"
    },
    {
        "leaf-length": 5.9,
        "leaf-width": 3,
        "petal-length": 5.1,
        "petal-width": 1.8,
        "class": "Iris-virginica"
    }
];
var brain = require('brain');
var net = new brain.NeuralNetwork();
var bodyParser = require("body-parser");
var express = require('express');
var app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var corellationTable = {
    "Iris-virginica": 3,
    "Iris-versicolor": 2,
    "Iris-setosa": 1};
var trainerSet = [];
sorceData.forEach(function (item) {
    var lol = {};
    lol[item.class]= 1;
    trainerSet.push({
        input: {
            "leaf-length": item["leaf-length"],
            "leaf-width": item["leaf-width"],
            "petal-length": item["petal-length"],
            "petal-width": item["petal-width"]
        },
        output: lol
    })
});

net.train(trainerSet);
//var neuroRun = net.toFunction();
var var1 = net.run({  "leaf-length": 5,
    "leaf-width": 3.6,
    "petal-length": 1.4,
    "petal-width": 0.2})
console.log(var1);
var var2 = net.run({ "leaf-length": 5.9,
    "leaf-width": 3,
    "petal-length": 5.1,
    "petal-width": 1.8})
console.log(var2);

var var2 = net.run({ "leaf-length": 5.9,
    "leaf-width": 5,
    "petal-length": 6.1,
    "petal-width": 1.8})
console.log(var2);

var var3 = net.run({ "leaf-length": 5.9,
    "leaf-width": 1,
    "petal-length": 1.1,
    "petal-width": 1.8})
console.log(var3);

function neuroRun(params) {
    return net.run(params);
}




app.get('/structure', function (req, res) {
    res.send(sorceData);
});

app.post('/flower',function(req,res){
    //var user_name=req.body;
    //var password=req.body.password;
    //net.run(req.body)
    //console.log("User name = "+user_name+", password is "+password);
    //net.train(trainerSet);

    //res.end('wow')
    setTimeout(function () {
        //app.close();
        var response = neuroRun({ "leaf-length": req.body["leaf-length"],
            "leaf-width": req.body["leaf-width"],
            "petal-length": req.body["petal-length"],
            "petal-width": req.body["petal-width"]});
        console.log(req.body);
        //res.write(200, response);
        //res.end('wow');
        // ^^^^^^^^^^^
    }, 300)

});


app.listen(3000, function () {
    console.log('Example app listening on port 3000!');

});